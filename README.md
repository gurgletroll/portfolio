pour ce portfolio, l'idée initiale était de partir sur 2 colonnes, l'une étant fixe avec un petit menu, mon nom et un espace de contacts, tandis que l'autre contient la présentation, les projets et les skills. J'ai essayé de mettre en œuvre plusieurs techniques de CSS qui nous avons vues ou que j'ai découvert au travers de mes recherches, et Bootstrap m'a été bien utile pour gérer des éléments de mise en page, le responsive, et inclure quelques éléments. 

à l'avenir, j'aimerais toutefois changer ce qui pour moi représente un léger défaut ergonomique : pour scroller la partie de droite, il faut que la souris soit dessus. C'est le résultat d'un choix structurel de la page. J'aurais pu essayer de tout laisser sur une seule colonne, mais cela aurait probablement soulevé d'autres difficultés. à voir si je peux gérer cela avec un peu de Typescript. D'après ce que j'ai vu des éléments de documentations c'est possible, mais je préfère attendre de bien maîtriser et comprendre le code avant de rajouter cette fonctionnalité. 

même si elle est très moche, et initialement faite au crayon, je suis globalement satisfait de ma maquette fonctionnelle, puisqu'elle m'a permis de savoir dans quelle direction aller et vers quels choix techniques m'orienter, car je savais comment les blocs devaient s'organiser pour un design responsive. 


![maquetteFonctionnelle](MaquetteFonctionnelle.jpg)


